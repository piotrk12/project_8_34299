
# Projekt nr 8 



## Opis projektu

Celem projektu jest stworzenie aplikacji do zamawiania jedzienia w pizzeri  drogą internetową, przy użyciu paru kliknięć. Głównym załozeniem jest stworzenie interfejsu przyjaznego dla klienta, bez zbędnych rozpraszających dodatków z minimalistycznym menu. 
Wszystkie potrzebne informacje takie jak menu czy promocje są przechowywane w plikach tekstowych co umożliwa szybką, łatwą i nie skomplikowaną zmiane bez ingerencji w kod aplikacji.  
Całość została zrealizowana przy użyciu javy.  



## Funkcjonalność  

(1) Menu  
![App Screenshot](https://i.imgur.com/4TIlDwT.png)

Do poruszania się po aplikacji wykorzystujemy
proste menu tekstowe.  
Wyboru odpowiednich opcji dokonujemy przez
wpisanie odpowieniego znaku widocznego po lewej 
przy każdej z opcji.  

  (2) Produkty  
![App Screenshot](https://i.imgur.com/QX0VhGf.png)

Produkty wybieramy zapomocą id 
widocznego po lewej stronie przy nazwie.  
Po wybraniu produktu program zapyta nas
o ilość zamawianej pozycji, 
a następnie czy chcemy opuścić menu wyboru 
czy chcemy dokonać ponownego dodania do koszyka.  

  (3) Do koszyka  
![App Screenshot](https://i.imgur.com/I82IGAu.png)  
  (4) Usuwanie  
![App Screenshot](https://i.imgur.com/nxKdwGS.png)

Użytkownik ma możliwość edycji koszyka.  
Tutaj program zapyta nas w pierwszej kolejności o produkt który chcemy edytować/usunąć 
następnie co dokładnie chcemy zrobić.  

  (5) Koszyk  
![App Screenshot](https://i.imgur.com/bROs40G.png)

Podglad koszyka

  (6) Paragon  
![App Screenshot](https://i.imgur.com/pJZAx7Y.png)

Po zakończeniu działania całego programu jest generowany Paragon.  
Paragon jest zapisywany w pliku tekstowym co umożliwia jego wydruk oraz podgląd w każdym momencie.  



## Szczegóły techniczne

Projekt wykonany w programie: **IntelliJ IDEA Comunity Edition**  
Język programowania: **Java**  
Wersja javy: **17.0.1**  



## Podział prac

| Kozioł Piotr      | Koper Mikołaj     |
| -------------     | -------------     |
| Prace nad kodem   | Prace nad kodem   |
| Debugowanie       | Debugowanie       | 
| Naprawa błędów    | Naprawa błędów    |
| Readme            | Udoskonalenia     |


 
 
 
## Autorzy

- Piotr Kozioł 34299
- Mikołaj Koper 34296
