import java.io.*;

/**
 * Receipt Class
 * Class used to create receipt in .txt file
 */
public class Receipt {

    /**
     * Constructor of Receipt Class
     */
    public Receipt() {}

    /**
     * Method used to delete existing file
     * @param fileName name of a file to delete
     */
    public void deleteFile(String fileName) {
        try {
            File file = new File(fileName);
            if (file.delete()) {
                //System.out.println(file.getName() + " zostal skasowany!");
            }
            else {
                //System.out.println("Operacja kasowania sie nie powiodla.");
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method used to write text to file Paragon.txt
     * @param text content to write into the receipt file
     */
    public void writeToReceipt(String text) {
        try(FileWriter fw = new FileWriter("Paragon.txt", false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)) {
            out.println(text);
            out.close();
            //System.out.println("Zapis do pliku ukończony powodzeniem");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
