import java.io.IOException;
import java.util.Scanner;

/**
 * Menu Class
 * Class used to print Pizzeria's menu and options on screen
 */
public class Menu {
    private static Pizzeria pizzeria;
    private static Scanner scanner;

    /**
     * menu() Method
     * Method used to handle the customer service
     * @throws IOException
     */
    public static void menu() throws IOException {
        pizzeria = new Pizzeria();
        scanner = new Scanner(System.in);
        int choice;

        // Printing Logo on screen
        pizza();
        System.out.println("\n-------- Pizzeria --------");

        do {
            System.out.printf("\nWybierz Opcje: \n");
            System.out.printf("1 Promocje\n");
            System.out.printf("2 Dodaj produkt\n");
            System.out.printf("3 Usuń produkt/zmień ilość\n");
            System.out.printf("4 Wyświetl koszyk\n");
            System.out.printf("5 Zamów\n");
            System.out.printf("0 Wyjście\n");
            System.out.println("");
            System.out.printf("Wybór: ");

            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    pizzeria.showPromotions();
                    pressToContinue();
                    break;
                case 2:
                    pizzeria.addProductToShopcart();
                    pizzeria.calculateCost();
                    break;
                case 3:
                    pizzeria.deleteProductFromShopcart();
                    break;
                case 4:
                    pizzeria.showShopcart();
                    pressToContinue();
                    break;
                case 5:
                    pizzeria.orderSummary();
                    pizzeria.clearShopcart();
                    pressToContinue();
                    break;
            }
        } while (choice != 0);

        printFreeSpace();
        System.out.println("\nŻyczymy smacznego !!!");
    }

    /**
     * Method used to wait for user input
     */
    public static void pressToContinue() {
        scanner.nextLine();
        String input = scanner.nextLine();
    }

    /**
     * Method used to print 10 lines of free space
     */
    public static void printFreeSpace() {
        System.out.print("\n\n\n\n\n\n\n\n\n\n");
    }

    /**
     * "Pizza time"
     */
    public static void pizza() {
        System.out.println("         _....._");
        System.out.println("     _.:`.--|--.`:._");
        System.out.println("   .: .'\\o  | o /'. '.");
        System.out.println("  // '.  \\ o|  /  o '.\\");
        System.out.println(" //'._o'. \\ |o/ o_.-'o\\\\");
        System.out.println(" || o '-.'.\\|/.-' o   ||");
        System.out.println(" ||--o--o-->|");
    }
}
