import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Pizzeria Class
 * Class that contains methods used to handling client service and order operations
 */
public class Pizzeria {
    private static NumberFormat formatter = new DecimalFormat("#0.00");
    private static ArrayList<Product> productArrayList = new ArrayList<>();
    private static ArrayList<Product> shoppingCart = new ArrayList<>();
    private Receipt receipt = new Receipt();
    double cost;
    String promotion;
    Scanner scanner = new Scanner(System.in);

    /**
     * Constructor of Pizzeria Class
     */
    public Pizzeria() throws FileNotFoundException {
        createProductsList();
    }

    /**
     * Method used to fill up the product list with products from .csv file
     * @throws FileNotFoundException
     */
    public void createProductsList() throws FileNotFoundException {
        Scanner input = new Scanner(new File("Menu"));
        input.useDelimiter(",");

        while (input.hasNext()) {
            String nazwa = input.next();
            String skład = input.next();
            double cena = Double.parseDouble(input.next());

            Product newProduct = new Product(nazwa, skład, cena);
            productArrayList.add(newProduct);
        }
    }

    /**
     * Method used to calculate product's quantity
     * @param a
     */
    public void productMenu(Product a) {
        int ilosc;
        System.out.println("Podaj ilość");
        ilosc = scanner.nextInt();
        a.setIlosc(a.getIlosc() + ilosc);
    }

    /**
     * Method used to show Shopping Cart content
     */
    public void showShopcart() {
        System.out.println("");
        for (Product x : shoppingCart) {
            System.out.println(x+"Ilość: "+x.getIlosc()+"\n");
        }
        System.out.println("-------------------------------");
        System.out.println("Suma: ");
        showCost();
    }

    /**
     * Method used to add product to the Shopping Cart
     */
    public void addProductToShopcart() {
        int i = 1;
        for (Product x : productArrayList) {
            System.out.println(i+"."+x);
            i++;
        }
        String choice;
        do {
            System.out.println("Podaj id produktu który chcesz dodać");
            int id = scanner.nextInt();
            Product a = productArrayList.get(id - 1);
            if(isEqual(a)){
            }else{
                    shoppingCart.add(a);
                    productMenu(a);
            }
            System.out.println("Czy chcesz Jeszcze coś zamówić [T/N]");
            choice = scanner.next();
        }while(!(choice.equals("N") || choice.equals("n")));
    }

    /**
     * Method used to delete product from the Shopping Cart
     */
    public void deleteProductFromShopcart() {
        int i = 1;
        for (Product x : shoppingCart) {
            System.out.println(i+"."+x+"ilość: "+x.getIlosc()+"\n");
            i++;
        }
        System.out.println("Podaj id produktu który chcesz usunąć (0-wyjście):");
        int id = scanner.nextInt();
        if(id != 0) {
            Product a = shoppingCart.get(id - 1);
            System.out.println("1 Edytuj ilość \n2 Usuń produkt");
            int choice = scanner.nextInt();
            if (choice == 2) {
                shoppingCart.remove(a);
            } else {
                int ilosc;
                System.out.println("Podaj ilość");
                ilosc = scanner.nextInt();
                a.setIlosc(ilosc);
            }
        }
    }

    /**
     * Method used to print current Promotions (from .csv file)
     * @throws FileNotFoundException
     */
    public void showPromotions() throws FileNotFoundException {
        Scanner input = new Scanner(new File("Promocje"));
        while (input.hasNextLine()) {
            String data = input.nextLine();
            System.out.println(data);
        }
    }

    /**
     * Method used to calculate Promotions
     */
    public void calculatePromotions() {
        if (productArrayList.get(2).getIlosc() >= 2) {
            cost -= productArrayList.get(2).getCena() * 0.5;
            promotion = "(-50 %)";
        } else if (cost > 100) {
            cost *= 0.8;
            promotion = "(-20 %)";
        } else if (shoppingCart.contains(productArrayList.get(2))) {
            productArrayList.get(7).setCena(0);
            shoppingCart.add(productArrayList.get(7));
            promotion = "";
        } else {
            promotion = "";
        }
    }

    /**
     * Method used to calculate Cost
     */
    public void calculateCost() {
        cost = 0;
        for (Product x : shoppingCart) {
            cost += x.getCena() * x.getIlosc();
        }
    }

    /**
     * Method used to print Cost on screen
     */
    public void showCost() {
        calculateCost();
        calculatePromotions();
        System.out.println(formatter.format(cost)+"zł "+ promotion);
    }

    /**
     * Method used to print products list on screen
     */
    public void showMenu() {
        for (Product x : productArrayList) {
            System.out.println(x);
        }
    }

    /**
     * Method used to compare products
     * @param product
     * @return
     */
    public boolean isEqual(Product product) {
        for (Product x: shoppingCart) {
            if (x.getNazwa().equals(product.getNazwa())) {
                if (x.getSkład().equals(product.getSkład()))
                    productMenu(x);
                return true;
            }
        }
        return false;
    }

    /**
     * Method used to print order summary on screen
     */
    public void orderSummary() {
        calculatePromotions();
        System.out.println("");

        String summary = "";
        for (Product x : shoppingCart) {
            summary += (x.getNazwa()+"\t"+x.getIlosc()+"x"+x.getCena()+"\t\t"+formatter.format(x.getIlosc()*x.getCena())+"\n");
        }
        System.out.print(summary);

        System.out.println("-------------------------------");
        System.out.println("kwota do zapłacenia: ");
        showCost();

        createReceipt(summary);
    }

    /**
     * Method used to create order Receipt
     * @param cart
     */
    public void createReceipt(String cart) {
        String text = "\n\n\n";
        text += "           PIZZERIA\n";
        text += "         Kzioł - Koper\n";
        text += "NIP: 3429934296\n";
        text += "     ## PARAGON FISKALNY ##\n";
        text += "-------------------------------\n";
        text += "Nazwa       Il/Cena        Suma\n";
        text += "-------------------------------\n";
        text += cart;
        text += "                    " + promotion + "\n";
        text += "-------------------------------\n";
        text += "SUMA:               " + String.valueOf(cost) + "\n";
        text += "-------------------------------\n";
        text += "     ## PARAGON FISKALNY ##\n";
        text += "-------------------------------\n";
        text += "Projekt Pizzeria  - NISP - 2022";

        receipt.writeToReceipt(text);
    }

    /**
     * Method used to clear Shopping Cart
     */
    public void clearShopcart() {
        shoppingCart.clear();
    }
}
