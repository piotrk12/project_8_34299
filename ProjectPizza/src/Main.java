import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Main Class
 * The main class used to run the Pizzeria App
 */
public class Main {

    /**
     * main() Method
     * Method used to start up the program
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
	    Menu.menu();
    }
}
