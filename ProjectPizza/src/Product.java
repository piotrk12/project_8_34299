import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Product Class
 * Class used to store products properties
 */
public class Product {
    private int ilosc = 0;
    private String nazwa;
    private String Skład;
    private double cena;
    private static NumberFormat formatter = new DecimalFormat("#0.00");

    /**
     * Constructor of Product Class
     *
     * @param nazwa product's name
     * @param skład product's composition
     * @param cena product's price
     */
    public Product(String nazwa, String skład, double cena) {
        this.nazwa = nazwa;
        this.Skład = skład;
        this.cena = cena;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) { this.ilosc = ilosc; }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getSkład() {
        return Skład;
    }

    public void setSkład(String skład) {
        Skład = skład;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    /**
     * toString() Method
     * @return product in name, composition, price format
     */
    @Override
    public String toString() {
            return String.format("Nazwa: %s\r\nSkład: %s\r\nCena: %s\r\n",
                     nazwa, Skład, formatter.format(cena));
    }
}
